# Passos para configurar o ambiente

1 - Gerar os certificados

openssl genrsa -out ca.key 2048

openssl req -new -x509 -days 365 -key ca.key -subj "/C=BR/ST=PR/L=Cascavel/O=Franciones Inc./CN=Franciones Root CA" -out ca.crt

openssl req -newkey rsa:2048 -nodes -keyout server.key -subj "/C=BR/ST=PR/L=Cascavel/O=Franciones Inc./CN=192.168.15.100" -out server.csr

openssl x509 -req -extfile <(printf "subjectAltName=IP:192.168.15.100") -days 365 -in server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out server.crt

2 - Criar container no Rancher da imagem `bradbeck/nexus-https` e parametrizar os volumes de `data` e `ssl`

3 - Copiar para o diretorio do volume onde foi configurado o `ssl` os arquivos `server.crt` como `cacert.pem` e o `server.key` como `cakey.pem`, sobrepondo se existir e apagar o keystore.key e jetty.key


# Configurar docker client

1 - Copiar o ca.crt gerado no passo 1 anterior para dentro do /etc/docker/certs.d/192.168.15.100:9999/



# bradbeck/nexus-https

A Dockerfile for Sonatype Nexus Repository Manager 3 with HTTPS support, based on CentOS.

GitHub Repository: https://github.com/bradbeck/nexus-https

This Dockerfile is loosely based on the following, please refer to it for additional configuration information: https://github.com/sonatype/docker-nexus3

To run, generating a default `keystore.jks`.

```
docker run -p 8443:8443 bradbeck/nexus-https
```

To run, binding the exposed ports (8081, 8443), data directory, and volume containing `keystore.jks`.

```
$ docker run -d -p 8081:8081 -p 8443:8443 -v ~/nexus-data:/nexus-data -v ~/nexus-ssl:/opt/sonatype/nexus/etc/ssl --name nexus bradbeck/nexus-https
```

To (re)build the image:

```
$ docker build --rm --tag=bradbeck/nexus-https .
```
## Environment Variables
Variable               | Default Value | Description
-----------------------|----------------------------------------|------------
`PUBLIC_CERT`          |`/opt/sonatype/nexus/etc/ssl/cacert.pem`|the fully qualified container path for the CA certificate
`PUBLIC_CERT_SUBJ`     |`/CN=localhost`                         |the subject used if the CA certificate is created
`PRIVATE_KEY`          |`/opt/sonatype/nexus/etc/ssl/cakey.pem` |the fully qualified container path for the private certificate key
`PRIVATE_KEY_PASSWORD` |`password`                  |the password for the private certificate key, used for `keystore.jks` if it is being generated

## Notes

* Default credentials are: `admin` / `admin123`

* Installation of Nexus is to `/opt/sonatype/nexus`.

* Nexus will expect to find a java keystore file at `/opt/sonatype/nexus/etc/ssl/keystore.jks` which
resides in the exposed volume `/opt/sonatype/nexus/etc/ssl`.
  * `entrypoint.sh` will create `keystore.jks` if it does not already exist.

* A persistent directory, `/nexus-data`, is used for configuration,
logs, and storage. This directory needs to be writable by the Nexus
process, which runs as UID 200.
